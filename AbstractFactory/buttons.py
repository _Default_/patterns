from abc import ABC, abstractmethod
from Overrider.overrider import override


class Button(ABC):
    @abstractmethod
    def paint(self):
        pass


class MacOSButton(Button):
    @override(Button)
    def paint(self):
        print('MacOSButton created.')


class WindowsButton(Button):
    @override(Button)
    def paint(self):
        print('WindowsButton created.')
