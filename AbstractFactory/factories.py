from abc import ABC, abstractmethod
from AbstractFactory.buttons import Button, MacOSButton, WindowsButton
from AbstractFactory.checkboxes import Checkbox, MacOSCheckbox, WindowsCheckbox
from Overrider.overrider import override


class GUIFactory(ABC):
    def create_button(self) -> Button:
        pass

    def create_checkbox(self) -> Checkbox:
        pass


class MacOSFactory(GUIFactory):
    @override(GUIFactory)
    def create_button(self) -> Button:
        return MacOSButton()

    @override(GUIFactory)
    def create_checkbox(self) -> Checkbox:
        return MacOSCheckbox()


class WindowsFactory(GUIFactory):
    @override(GUIFactory)
    def create_button(self) -> Button:
        return WindowsButton()

    @override(GUIFactory)
    def create_checkbox(self) -> Checkbox:
        return WindowsCheckbox()
