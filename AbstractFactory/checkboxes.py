from abc import ABC, abstractmethod
from Overrider.overrider import override


class Checkbox(ABC):
    @abstractmethod
    def paint(self):
        pass


class MacOSCheckbox(Checkbox):
    @override(Checkbox)
    def paint(self):
        print('MacOSCheckbox created.')


class WindowsCheckbox(Checkbox):
    @override(Checkbox)
    def paint(self):
        print('WindowsCheckbox created.')
