from AbstractFactory.buttons import Button
from AbstractFactory.checkboxes import Checkbox
from AbstractFactory.factories import GUIFactory, WindowsFactory, MacOSFactory
import platform

class Application:
    __button: Button
    __checkbox: Checkbox

    def __init__(self, factory: GUIFactory):
        self.__button = factory.create_button()
        self.__checkbox = factory.create_checkbox()

    def paint(self):
        self.__button.paint()
        self.__checkbox.paint()


def configure_app() -> Application:
    os = str(platform.architecture()).lower()
    if 'win' in os:
        factory = WindowsFactory()
    else:
        factory = MacOSFactory()
    return Application(factory)


app = configure_app()
app.paint()


