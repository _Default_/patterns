from Facade.gps_function import *


class GPSInterface:

    __power = GPSPower()
    __notifier = GPSNotifier()
    __advisor = RoadAdvisor()

    def __init__(self):
        pass

    def activate(self) -> None:
        self.__power.power_on()
        self.__notifier.download_road_info()
        self.__advisor.route()

    def deactivate(self) -> None:
        self.__power.power_off()


class Facade:

    __a: int
    __gps = GPSInterface()

    def __init__(self, a: int):
        self.__a = a

    def run(self):
        if self.__a == 1:
            self.__gps.activate()
        if self.__a == 2:
            self.__gps.deactivate()

