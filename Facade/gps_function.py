class GPSPower:

    @staticmethod
    def power_on() -> None:
        print("GPS включен.")

    @staticmethod
    def power_off() -> None:
        print("GPS выключен.")


class GPSNotifier:

    @staticmethod
    def download_road_info() -> None:
        print("Загрузка необходимой информации...")
        print("Загрузка завершена.")


class RoadAdvisor:

    @staticmethod
    def route() -> None:
        print("Прокладываю маршрут.")
