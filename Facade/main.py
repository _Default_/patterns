from Facade.facade import *
button1 = Facade(1)
button2 = Facade(2)
button1.run()
button2.run()