from Overrider.overrider import override
from abc import ABC, abstractmethod


class Cars(ABC):
    name: str = 'No Model'

    def get_info(self):
        return self.name

    @abstractmethod
    def get_price(self) -> int:
        pass


class VwTouareg(Cars):
    def __init__(self):
        self.name = 'Volkswagen Touareg'

    @override(Cars)
    def get_price(self) -> int:
        return 10000


class VwPassat(Cars):
    def __init__(self):
        self.name = 'Volkswagen Passat'

    @override(Cars)
    def get_price(self) -> int:
        return 5000


