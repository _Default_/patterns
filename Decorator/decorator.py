from Decorator.cars import *
from abc import abstractmethod, ABC


class Decorator(Cars, ABC):
    @abstractmethod
    def get_info(self):
        pass


class GPS(Decorator):
    __car: Cars

    def __init__(self, car: Cars):
        self.__car = car

    @override(Decorator)
    def get_info(self):
        return self.__car.get_info() + ' + GPS'

    @override(Decorator)
    def get_price(self) -> int:
        return self.__car.get_price() + 500


class AirCondition(Decorator):
    __car: Cars

    def __init__(self, car: Cars):
        self.__car = car

    @override(Decorator)
    def get_info(self):
        return self.__car.get_info() + ' + Кондиционер'

    @override(Decorator)
    def get_price(self) -> int:
        return self.__car.get_price() + 1000
