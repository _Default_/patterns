from Decorator.cars import VwTouareg, VwPassat
from Decorator.decorator import GPS, AirCondition

touareg = VwTouareg()
print(touareg.get_info())
print(touareg.get_price())

touareg = GPS(touareg)
print(touareg.get_info())
print(touareg.get_price())

touareg = AirCondition(touareg)
print(touareg.get_info())
print(touareg.get_price())

passat = GPS(AirCondition(VwPassat()))
print(passat.get_info())
print(passat.get_price())