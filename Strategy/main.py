from Strategy.strategy import Strategy1, Strategy2
from Strategy.context import Context

ctx = Context()
string = str(input("Введите массив\n"))
method = int(input("Выберите способ сортировки:\n1-по возрастанию\n2-по убыванию\n"))
data = [int(i) for i in string.split(' ')]
if method == 1:
    ctx.set_strategy(Strategy1())
elif method == 2:
    ctx.set_strategy(Strategy2())
else:
    print('Неверная команда.')
    exit(-1)
data = ctx.execute_strategy(data)
print(data)
