from Strategy.strategy import InterfaceStrategy


class Context:
    __strategy: InterfaceStrategy

    def __init__(self):
        pass

    def set_strategy(self, strategy: InterfaceStrategy):
        self.__strategy = strategy

    def execute_strategy(self, data: list):
        return self.__strategy.execute(data)