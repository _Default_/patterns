from abc import ABC, abstractmethod
from typing import List

from Overrider.overrider import override


class InterfaceStrategy(ABC):
    @abstractmethod
    def execute(self, data: list) -> list:
        pass


class Strategy1(InterfaceStrategy):
    @override(InterfaceStrategy)
    def execute(self, data: list) -> list:
        return sorted(data)


class Strategy2(InterfaceStrategy):
    @override(InterfaceStrategy)
    def execute(self, data: list) -> list:
        return sorted(data, reverse=True)
