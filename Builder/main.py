from Builder.builder import Builder
from Builder.director import Director

director = Director()
builder = Builder()
director.construct_sport_car(builder=builder)
car1 = builder.build()
print("Создан автомобиль: " + str(car1.get_type()) + " " + str(car1.get_transmission()))
director.construct_city_car(builder=builder)
car1 = builder.build()
print("Создан автомобиль: " + str(car1.get_type()) + " " + str(car1.get_transmission()))