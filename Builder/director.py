from Builder.builder import Builder
from Builder.car import Type, Engine, Transmission, GPS


class Director:
    def __init__(self):
        pass

    @staticmethod
    def construct_sport_car(builder: Builder):
        builder.set_type(Type.SPORT_CAR)
        builder.set_seats(2)
        builder.set_engine(Engine(3.0))
        builder.set_transmission(Transmission.AUTOMATIC)
        builder.set_gps(GPS())

    @staticmethod
    def construct_city_car(builder: Builder):
        builder.set_type(Type.CITY_CAR)
        builder.set_seats(4)
        builder.set_engine(Engine(1.5))
        builder.set_transmission(Transmission.MANUAL)
        builder.set_gps(GPS())
