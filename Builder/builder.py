from abc import ABC, abstractmethod
from Builder.car import Type, Engine, Transmission, GPS, Car
from Overrider.overrider import override


class InterfaceBuilder(ABC):
    @abstractmethod
    def set_type(self, t: Type) -> None:
        pass

    @abstractmethod
    def set_seats(self, seats: int) -> None:
        pass

    @abstractmethod
    def set_engine(self, engine: Engine) -> None:
        pass

    @abstractmethod
    def set_transmission(self, transmission: Transmission) -> None:
        pass

    @abstractmethod
    def set_gps(self, gps: GPS) -> None:
        pass


class Builder(InterfaceBuilder):
    __type: Type
    __seats: int
    __engine: Engine
    __transmission: Transmission
    __gps: GPS

    @override(InterfaceBuilder)
    def set_type(self, t: Type) -> None:
        self.__type = t

    @override(InterfaceBuilder)
    def set_seats(self, seats: int) -> None:
        self.__seats = seats

    @override(InterfaceBuilder)
    def set_engine(self, engine: Engine) -> None:
        self.__engine = engine

    @override(InterfaceBuilder)
    def set_transmission(self, transmission: Transmission) -> None:
        self.__transmission = transmission

    @override(InterfaceBuilder)
    def set_gps(self, gps: GPS) -> None:
        self.__gps = gps

    def build(self) -> Car:
        return Car(self.__type, self.__seats, self.__engine, self.__transmission, self.__gps)
