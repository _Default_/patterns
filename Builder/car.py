from enum import Enum


class Type(Enum):
    CITY_CAR = 1
    SPORT_CAR = 2


class Engine:
    __mileage: float
    __started: bool

    def __init__(self, mileage: float):
        self.__mileage = mileage


class Transmission(Enum):
    MANUAL = 1
    AUTOMATIC = 2


class GPS:
    __route: str

    def __init__(self):
        pass

    def set_route(self, route: str):
        self.__route = route


class Car:
    __type: Type
    __seats: int
    __engine: Engine
    __transmission: Transmission
    __gps: GPS
    __fuel: float

    def __init__(self, t: Type, seats: int, engine: Engine, transmission: Transmission, gps: GPS):
        self.__type = t
        self.__seats = seats
        self.__engine = engine
        self.__transmission = transmission
        self.__gps = gps

    def get_type(self):
        return self.__type

    def get_transmission(self):
        return self.__transmission